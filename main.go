package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type Metric struct {
	Value int
	Time  time.Time
}

// LocalMetricStore is an in-memory store of metrics
type LocalMetricStore struct {
	Data map[string][]Metric
}

// Reportable returns "true" if a metric is less than an hour old.
func (m *Metric) Reportable() bool {
	hour, _ := time.ParseDuration("1h")

	if time.Now().Sub(m.Time) > hour {
		return false
	}

	return true
}

// SumReportable returns the sum of Metrics (0 or more) with the given key
// that fall within the reporting window.
func (s *LocalMetricStore) SumReportable(key string) int {
	var sum int

	if _, i := store.Data[key]; i {
		reportableMetrics := []Metric{}
		for _, val := range store.Data[key] {
			if val.Reportable() == true {
				sum += val.Value
				reportableMetrics = append(reportableMetrics, val)
			}
		}
		// Metrics that are not reportable are discarded
		store.Data[key] = reportableMetrics
	}

	return sum
}

var store LocalMetricStore

type PostValue struct {
	Val string `json:"value"`
}

// PostMetricHandler handles new metric POST requests.
// Response is 200:OK.
func PostMetricHandler(w http.ResponseWriter, r *http.Request) {
	var metric Metric
	var value PostValue
	vars := mux.Vars(r)
	key := vars["key"]

	if err := json.NewDecoder(r.Body).Decode(&value); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	metricVal, err := strconv.Atoi(value.Val)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	metric.Value = metricVal
	metric.Time = time.Now()

	if _, k := store.Data[key]; k {
		store.Data[key] = append(store.Data[key], metric)
	} else {
		storeValue := []Metric{metric}
		store.Data[key] = storeValue
	}

	return
}

type MetricSum struct {
	Value int `json:"value"`
}

// GetMetricHandler returns metrics with a given key that are in the reporting window.
// Response is "{0}" if there are zero metrics for the provided key.
func GetMetricHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var resp MetricSum
	vars := mux.Vars(r)
	key := vars["key"]

	resp.Value = store.SumReportable(key)

	response, err := json.Marshal(resp)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.Write(response)
	return
}

func main() {
	store = LocalMetricStore{Data: make(map[string][]Metric)}
	port := ":8000"

	fmt.Println("serving metrics at ", port)

	r := mux.NewRouter()
	r.HandleFunc("/metric/{key}", PostMetricHandler).Methods("POST")
	r.HandleFunc("/metric/{key}/sum", GetMetricHandler).Methods("GET")
	log.Fatal(http.ListenAndServe(port, r))
}
