# Sum Metric Service

A small metric logging and reporting service that sums metrics by time window for the most recent hour.

## Notes
- Uses gorilla/mux package
- Service runs a port 8000
- If a key does not exist, a sum of 0 is returned
- Metrics older than one hour are discarded when a sum is requested

## Endpoints
Request:  
`POST /metric/{key} value=1`  
Response:  
`200 OK`   

Request:  
`GET /metric{key}/sum`  
Response:  
`200 OK`  
`{ "value": 1 }`

## Improvements
- Scrub the metrics store periodically and discard data outside of the reporting window. This would improve performance for metrics that are infrequently accessed.
- Use interfaces for the metric store to easily allow other data stores to be used.

